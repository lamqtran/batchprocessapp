include:
  - project: UKI/systems/service-architecture/deployment/gitlab-ci-plugins/maven
    file: plugin.yml
    ref: 1.0.1
  - project: UKI/systems/service-architecture/deployment/gitlab-ci-plugins/docker
    ref: 1.2.4
    file: plugin.yaml 
  - project: UKI/systems/service-architecture/deployment/gitlab-ci-plugins/clair
    file: plugin.yml
    ref: 1.0.0
  - project: UKI/systems/service-architecture/deployment/gitlab-ci-plugins/helm-charts
    file: plugin.yml
    ref: feature-lint
  - project: UKI/systems/service-architecture/deployment/gitlab-ci-plugins/helmfile
    file: plugin.yml
    ref: 2.0.1
  - project: UKI/systems/service-architecture/deployment/gitlab-ci-plugins/cxsast
    file: plugin.yml
    ref: 2.2.3


stages:
  - test
  - build
  - release
  - container_scanning
  - deploy_dev
  - destroy_dev

.auto_devops_charts: &auto_devops_charts  |

  function setup_env_variables() {
   export CLUSTER_LIFECYCLE=$CI_ENVIRONMENT_NAME
    export HELM_INDEX_FILE="index.yaml"
    export CHART_PATH=`find . -maxdepth 3 -name "Chart.yaml" -exec dirname {} \; | head -n 1 | xargs realpath`
    printf "Chart path resolved to %s\n" $CHART_PATH
    if [[ $CI_COMMIT_REF_NAME == "master" ]] || [[ ! -z $CI_BUILD_TAG ]];
    then
      export RELEASE_ENV_NAME=stable
      export CHART_REPO_LIFECYCLE="stable"
    else
      export CI_APPLICATION_TAG=$CI_COMMIT_REF_SLUG
      temp_release_env_name=${CI_COMMIT_REF_NAME}
      export RELEASE_ENV_NAME="${temp_release_env_name//[_]/-}"
      export CHART_REPO_LIFECYCLE="incubator"
    fi
  }
 

#-----------------------------
# Runners definition
# -----------------------------

.london-openstack-runner:
  tags: &london-openstack-runner
    - linux
    - london
    - privileged

.boca-privileged:
  tags: &boca-privileged-runner
    - boca
    - privileged
.boca-runner:
  tags: &boca-runner
    - boca
    - boca-dev
    - docker

# -----------------------------
# Variables definition
# -----------------------------
.var-common:
  variables: &var-common
    # destination namespace       
    SERVICE_NAMESPACE: ins-stem
    # label to be picked up by alert manager
    SERVICE_ALERT_LABEL: stem-ops
    # docker plugin variables
    DOCKER_FILE: $CI_PROJECT_DIR/docker/Dockerfile
    # helmfile plugin variables
    HELMFILE_PATHS: helmfile.d/helmfile.yaml
    HELMFILE_HELM3: "1"
    HELMFILE_LOG_LEVEL: info
    HELMFILE_ENVIRONEMNT: ${CI_ENVIRONMENT_NAME}
    ADFS_REGION: eu-west-1
    ADFS_HOST: federation.reedelsevier.com
    ADFS_PROVIDER_ID: urn:amazon:webservices
    ADFS_SESSION_DURATION: 3600
    ADFS_PROFILE: default
     
.var-dev:
  variables: &var-dev
    <<: *var-common
    # helmfile plugin variables:
    KUBE_CLUSTER_NAME: ris-insuki-shared-dev1
    ADFS_ROLE_ARN: arn:aws:iam::626039715452:role/ADFS-K8SAdministrators
    # host where your service will be available
    HOST: eu-west-1.ris-insuki-shared-dev1.lexisnexisrisk.io


# -----------------------------
# HELM CHART LINT AND BUILD
# ----------------------------- 
chart/lint:
  extends: .plugin-helm-charts-lint
  stage: test
  tags: *boca-privileged-runner

chart/build:
  extends: .plugin-helm-charts-build
  stage: build
  tags: *boca-privileged-runner
  before_script:
    - *auto_devops_charts
    - setup_env_variables
  variables:
    CT_CHART_DIRS: "charts"
    CHART_REPO_LIFECYCLE: "replaced in setup_env_variables"
  except:
    - tags

# -----------------------------
# BUILD AND RELEASE DOCKER IMAGE  
# -----------------------------
docker_release:
  extends: .plugin-docker-dind
  stage: release
  variables:
    <<: *var-common
  before_script:
    - *auto_devops_charts
    - setup_env_variables

      
# -----------------------------
# CONTAINER SCAN  
# -----------------------------
clair/clair-scan:
  image: gitlab.ins.risk.regn.net:4567/uki/systems/service-architecture/deployment/gitlab-ci-plugins/docker:1.2.1
  extends:
    - .plugin-docker-set-clair-env
    - .plugin-clair-scan
  stage: container_scanning  
    

# -----------------------------
# Checkmarx SAST scan
# -----------------------------
cxsast/cxsast-scan:
  extends:
    - .plugin-cxsast
  stage: test
  dependencies: []
  rules:
  - if: $CI_COMMIT_BRANCH == 'master'
    when: always
  - when: manual
    allow_failure: true

#-----------------------------




# -----------------------------
# GENERIC DEPLOYMENT 
# -----------------------------
.deploy:
  extends: .plugin-helmfile-apply
  tags: *boca-privileged-runner
  before_script:
    - *auto_devops_charts
    - setup_env_variables
  when: always
  
.destroy:
  extends: .plugin-helmfile-destroy
  before_script:
    - *auto_devops_charts
    - setup_env_variables
  when: always  

# -----------------------------
# DEV DEPLOYMENT 
# -----------------------------
deploy.dev:
  extends: .deploy
  stage: deploy_dev
  variables:
    <<: *var-dev
  environment:
    name: dev
    url: http://${HOST}  
  only:
    variables:
      - $CI_COMMIT_REF_NAME == "master"

deploy.dev.manual:
  extends: .deploy
  stage: deploy_dev
  variables:
    <<: *var-dev
  environment:
    name: dev
    url: http://${HOST}
  when: manual
  only:
    variables:
      - $CI_COMMIT_REF_NAME != "master"

destroy.dev:
  extends: .destroy
  stage: destroy_dev
  variables:
    <<: *var-dev
  environment:
    name: dev
    url: http://${HOST}
  when: manual
