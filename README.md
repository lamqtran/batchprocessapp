# project-template

Change this README.md file as your Application requires.

## Default Configuration
To configure project created from this template clone it to a local environment and run script

```bash
./init.sh
```
script will edit files and change paths based on project diretory name. Script itself will get deleted in the last step.
Once finished, project is ready to be imported in developement IDE as a maven project.


## Optional Parameters

script takes 2 optional parameters to override default values:

```bash
./init.sh [project_name package_name]
```

Where:
* `project_name`
    * is string to replace "project-template" strings from original tempate and it is used as an application name in various places (pom.xml, chart.yaml, application.yaml etc. )
    * if empty, value is project directory name
* `package_name`
    * value should always start with `com.lnrs` , in order for stem-core functionality to work properly. Use different value only if you fully understand the impact.
    * it is used as package name replacement for `com.lnrs.template` in a template
    * if empty, value is `com.lnrs.${suffix}`, where suffix is derived from the project directory name by switching to lower case and remove kebab case


## Manual Configuration
There are a few changes you need to make to this project-template in order to get your Application up and running.
These changes can be identified by doing a search on "**STEM_CONFIGURATION_REQUIRED**" and changing the contents accordingly.

Additionally two directories need to renamed from "project-template" to something more befitting to your own project.
* change package name from *com.lnrs.template* to *com.lnrs.<yourAppNam>*
* change the chart name from *charts/project-template* to *charts/<yourAppChartName>*   


Please refer also to confluence page: 
[Project Set-Up](https://confluence.rsi.lexisnexis.com/display/UKIRE/STEM+-+2.+Project+Set-Up)

## Helm Chart
To install the project as a helm chart, refer to:
/charts/project-template/README.md