package com.lnrs.stem.central.batch.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.lnrs.stem.api.beans.StemRequestContext;
import com.lnrs.stem.api.factory.StemResponseFactory;
import com.lnrs.stem.api.models.StemRequest;
import com.lnrs.stem.api.models.StemResponse;
import com.lnrs.stem.central.batch.model.Echo;


/**
 * Test class for EchoController
 * This should deleted and replaced by application specific tests
 */


@ExtendWith(SpringExtension.class)
public class EchoControllerTest {

	@TestConfiguration
	@Import({EchoController.class})
	static class Config {}
	
	
	@MockBean
	private StemResponseFactory stemResponseFactory;
	
	@MockBean
	private BuildProperties buildProperties;
	
	@MockBean
	private StemRequestContext requestContext;
	
	@Autowired
	private EchoController echoController;
	
	
	private static final String ECHO_STRING = "testEchoString";
	private static final Echo ECHO_ENTITY = new Echo(ECHO_STRING);
	StemResponse<Echo> stemResp;
	
	@BeforeEach
    public void setUp() {
		stemResp = new StemResponse<Echo>();
		stemResp.setContent(ECHO_ENTITY);		
    }
	
	@Test
	public void getEcho() throws Exception {
		
		Mockito.when(stemResponseFactory.createStemResponse(Mockito.any(StemRequestContext.class), Mockito.any(Echo.class)))
		.thenReturn(stemResp);
		
		// key method to test
		ResponseEntity<StemResponse<Echo>> response = echoController.echo("some string");
		
		// check if method was called
		Mockito.verify(stemResponseFactory).createStemResponse(Mockito.any(StemRequestContext.class), Mockito.any(Echo.class));
		// check status 
		assertEquals(HttpStatus.OK, response.getStatusCode());
		// check content
		assertEquals(stemResp, response.getBody());
	}


	@Test
	public void postEcho() throws Exception {
		StemRequest<Echo> stemRequest = new StemRequest<Echo>(ECHO_ENTITY);
		Mockito.when(stemResponseFactory.createStemResponse(Mockito.any(StemRequestContext.class),Mockito.any(Echo.class)))
		.thenReturn(stemResp);
		
		// key method to test
		ResponseEntity<StemResponse<Echo>> response = echoController.echo(stemRequest);
		
		//check
		Mockito.verify(stemResponseFactory).createStemResponse(requestContext, ECHO_ENTITY);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(stemResp, response.getBody());
	}


}