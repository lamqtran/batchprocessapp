package com.lnrs.stem.central.batch.service;

import com.lnrs.stem.api.models.StemRequest;
import com.lnrs.stem.api.models.StemResponse;
import com.lnrs.stem.central.batch.bean.RequestDataLexId;
import com.lnrs.stem.central.batch.bean.ResponseDataLexId;

public interface DataSourceCaller {
	StemResponse<ResponseDataLexId> callLexIdService(StemRequest<RequestDataLexId> requestDataLexId);
}
