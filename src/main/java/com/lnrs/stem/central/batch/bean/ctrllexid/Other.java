package com.lnrs.stem.central.batch.bean.ctrllexid;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Other {

    private List<String> telephones;

    private String policyNumber;

    private String email;

    private String vrn;

    private String drivingLicenseNumber;
}
