package com.lnrs.stem.central.batch.job.process;

import com.lnrs.stem.central.batch.bean.eventupload.RequestDataUpload;
import com.lnrs.stem.central.batch.job.listener.JobCompletionListener;
import com.lnrs.stem.central.batch.service.DataSourceCaller;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.async.AsyncItemProcessor;
import org.springframework.batch.integration.async.AsyncItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;


@Configuration
@RequiredArgsConstructor
@Slf4j
public class JobProcessConfig {

	@Autowired
	DataSourceCaller dataSourceCaller;

	private final ResourceLoader resourceLoader;

	@Bean
	public Job jobProcess(JobBuilderFactory jobBuilderFactory,
						  StepBuilderFactory stepBuilderFactory,
						  FlatFileItemReader itemReaderReadFile,
						  AsyncItemProcessor asyncProcessor,
						  AsyncItemWriter asyncWriter) {
		log.info("Job");

		Step step = stepBuilderFactory.get("ETL-file-load")
				.<RequestDataUpload, RequestDataUpload>chunk(2)
				.reader(itemReaderReadFile)
				.processor(asyncProcessor)
				.writer(asyncWriter())
				.taskExecutor(taskExecutor())
				.build();


		return jobBuilderFactory.get("ETL-Load")
				.incrementer(new RunIdIncrementer())
				.preventRestart()
				.listener(listener())
				.start(step)
				.build();
	}

	@Bean
	public AsyncItemProcessor<RequestDataUpload, RequestDataUpload> asyncProcessor(ProcessService processService) {
		AsyncItemProcessor asyncItemProcessor = new AsyncItemProcessor();
		asyncItemProcessor.setTaskExecutor(taskExecutor());
		asyncItemProcessor.setDelegate(processService);
		return asyncItemProcessor;
	}

	@Bean
	public AsyncItemWriter<RequestDataUpload> asyncWriter() {
		AsyncItemWriter<RequestDataUpload> asyncItemWriter = new AsyncItemWriter<>();
		asyncItemWriter.setDelegate(itemWriter());
		return asyncItemWriter;
	}


	@Bean
	public FlatFileItemWriter<RequestDataUpload> itemWriter() {

		return new FlatFileItemWriterBuilder<RequestDataUpload>()
				.name("Writer")
				.append(false)
				.resource(new FileSystemResource("transactions.txt"))
				.lineAggregator(new DelimitedLineAggregator<>() {
					{
						setDelimiter(";");
						setFieldExtractor(new BeanWrapperFieldExtractor<>() {
							{
								setNames(new String[]{"resolver", "addressLine1", "addressLine2", "addressLine3", "addressLine4",
										"addressLine5", "addressLine6", "postalCode", "countryCode", "lineOfBusiness", "pointOfRequest",
										"insurerABICode", "scoreThreshold", "cridConfiguration", "lexIDOnly", "title", "forename", "middleNames", "surname", "dateOfBirth"});
							}
						});
					}
				})
				.build();
	}


	@Bean
	@StepScope
	public FlatFileItemReader<RequestDataUpload> itemReaderReadFile(@Value("#{jobParameters['pathFile']}") String pathFile) {
		log.info("itemReaderReadFile");

		FlatFileItemReader<RequestDataUpload> flatFileItemReader = new FlatFileItemReader<>();
		flatFileItemReader.setName("CSV-Reader");
		flatFileItemReader.setLinesToSkip(1);
		flatFileItemReader.setResource(new PathResource(pathFile));
		flatFileItemReader.setLineMapper(lineMapperAddress());
		return flatFileItemReader;
	}

	@Bean
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(64);
		executor.setMaxPoolSize(64);
		executor.setQueueCapacity(64);
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		executor.setThreadNamePrefix("MultiThreaded-");
		return executor;
	}

	@Bean
	public LineMapper<RequestDataUpload> lineMapperAddress() {

		DefaultLineMapper<RequestDataUpload> defaultLineMapper = new DefaultLineMapper<>();
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

		lineTokenizer.setDelimiter(",");
		lineTokenizer.setStrict(false);
		lineTokenizer.setNames("resolver", "addressLine1", "addressLine2", "addressLine3", "addressLine4",
				"addressLine5", "addressLine6", "postalCode", "countryCode", "lineOfBusiness", "pointOfRequest",
				"insurerABICode", "scoreThreshold", "cridConfiguration", "lexIDOnly", "title", "forename", "middleNames", "surname", "dateOfBirth");

		BeanWrapperFieldSetMapper<RequestDataUpload> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(RequestDataUpload.class);

		defaultLineMapper.setLineTokenizer(lineTokenizer);
		defaultLineMapper.setFieldSetMapper(fieldSetMapper);

		return defaultLineMapper;
	}


	@Bean
	public JobExecutionListener listener() {
		return new JobCompletionListener();
	}
}
