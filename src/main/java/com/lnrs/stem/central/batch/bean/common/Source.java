package com.lnrs.stem.central.batch.bean.common;

public enum Source {

	DEFERRED("deferred"),
	CACHE("cache"),
	PROCESS("process");

	private final String text;

	Source(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
}
