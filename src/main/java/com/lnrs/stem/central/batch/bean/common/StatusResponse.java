package com.lnrs.stem.central.batch.bean.common;

public enum StatusResponse {

	C("Complete-Found"),
	N("Complete-Not Found. Not possible to resolve an address from the information provided"),
	P("Complete-Not Found. Not possible to resolve to a single address from the information provided"),
	E("SVC-404: Error");

	private final String message;

	StatusResponse(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public static StatusResponse valueOf(Integer hwoElement) {
		if (hwoElement == null || hwoElement < 0) {
			return E;
		} else if (hwoElement == 0) {
			return N;
		} else if (hwoElement == 1) {
			return C;
		} else {
			return P;
		}
	}
}
