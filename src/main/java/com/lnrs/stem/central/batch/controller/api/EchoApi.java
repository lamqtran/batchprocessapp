package com.lnrs.stem.central.batch.controller.api;

import org.springframework.http.ResponseEntity;

import com.lnrs.stem.api.models.StemErrorResponse;
import com.lnrs.stem.api.models.StemRequest;
import com.lnrs.stem.api.models.StemResponse;
import com.lnrs.stem.central.batch.model.Echo;

//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
//Change swaggerConfig to be in line with project
import io.swagger.v3.oas.annotations.responses.ApiResponses;

public interface EchoApi {

	@Operation(description = "Return Stem-Response with content of that from URL path param", summary = "echo back a string in response body")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "OK. Response body will contain echo string."),
			@ApiResponse(responseCode = "400", description = "Invalid request.", content = @Content(schema = @Schema(implementation = StemErrorResponse.class))),
			@ApiResponse(responseCode = "500", description = "Stem Error", content = @Content(schema = @Schema(implementation = StemErrorResponse.class)))})

	ResponseEntity<StemResponse<Echo>> echo(String echo) throws Exception;

	@Operation(description = "Return Stem-Response as echo of Stem-Request", summary = "echo back a string in response body")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK. Stem-Response body content is that of Stem-Request body content."),
			@ApiResponse(responseCode = "400", description = "Invalid request.", content = @Content(schema = @Schema(implementation = StemErrorResponse.class))),
			@ApiResponse(responseCode = "500", description = "Stem Error", content = @Content(schema = @Schema(implementation = StemErrorResponse.class)))})

			ResponseEntity<StemResponse<Echo>> echo(StemRequest<Echo> echo) throws Exception;

}