package com.lnrs.stem.central.batch.bean;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseDataLexId {
	private String trackingNumber;
	private String productVersion;
	private String status;
	private String message;
	private String source;
	private String lexID;
	private String crid;
	private String matchCodes;
	private String matchType;
	private String confidenceScore;
	private String lineOfBusiness;

	public List<String> toArray() {
		List<String> stringList = new ArrayList<>();
		stringList.add(trackingNumber);
		stringList.add(productVersion);
		stringList.add(status);
		stringList.add(message);
		stringList.add(source);
		stringList.add(lexID);
		stringList.add(crid);
		stringList.add(matchCodes);
		stringList.add(matchType);
		stringList.add(confidenceScore);
		stringList.add(lineOfBusiness);

		return stringList;
	}
}
