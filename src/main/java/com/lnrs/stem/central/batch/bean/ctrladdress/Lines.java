package com.lnrs.stem.central.batch.bean.ctrladdress;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Lines {

	private Object line1;

	private Object line2;

	private Object line3;

	private Object line4;

	private Object line5;
}
