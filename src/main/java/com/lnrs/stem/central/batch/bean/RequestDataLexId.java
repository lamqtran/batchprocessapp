package com.lnrs.stem.central.batch.bean;

import com.lnrs.stem.central.batch.bean.ctrladdress.Address;
import com.lnrs.stem.central.batch.bean.ctrllexid.Configuration;
import com.lnrs.stem.central.batch.bean.ctrllexid.Name;
import com.lnrs.stem.central.batch.bean.ctrllexid.Other;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RequestDataLexId {

	private Configuration configuration;

	private Name name;

	private String dateOfBirth;

	private Address address;

	private Other other;
}
