package com.lnrs.stem.central.batch.job.reader;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.lnrs.stem.central.batch.bean.eventupload.RequestDataUpload;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.SynchronizedItemStreamReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReaderProcess extends SynchronizedItemStreamReader<RequestDataUpload> implements StepExecutionListener {

	@Autowired
	private AmazonS3 amazonS3Client;

	private final ResourceLoader resourceLoader;

	private static final String[] LINE_MAPPER = new String[]{"resolver", "addressLine1", "addressLine2", "addressLine3", "addressLine4",
			"addressLine5", "addressLine6", "postalCode", "countryCode", "lineOfBusiness", "pointOfRequest",
			"insurerABICode", "scoreThreshold", "cridConfiguration", "lexIDOnly", "title", "forename", "middleNames", "surname", "dateOfBirth"};

	public ReaderProcess(AmazonS3 amazonS3Client, ResourceLoader resourceLoader) throws IOException {
		super();
		this.amazonS3Client = amazonS3Client;
		this.resourceLoader = resourceLoader;
		this.setDelegate(synchronizedItemStreamReader());
	}

	public SynchronizedItemStreamReader<RequestDataUpload> synchronizedItemStreamReader() throws IOException {
		SynchronizedItemStreamReader<RequestDataUpload> synchronizedItemStreamReader = new SynchronizedItemStreamReader<RequestDataUpload>();
		List<Resource> resourceList = new ArrayList<>();
		String sourceBucket = "hieuvo-bucket-demo";
		String sourceObjectPrefix = "requestData.csv";
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
				.withBucketName(sourceBucket)
				.withPrefix(sourceObjectPrefix);
		ObjectListing sourceObjectsListing;
		do {
			sourceObjectsListing = amazonS3Client.listObjects(listObjectsRequest);
			for (S3ObjectSummary sourceFile : sourceObjectsListing.getObjectSummaries()) {

				if (!(sourceFile.getSize() > 0) || !(sourceFile.getKey().endsWith(".".concat("csv")))) {
					// Skip if file is empty (or) file extension is not "csv"
					continue;
				}
				resourceList.add(resourceLoader.getResource("s3://".concat(sourceBucket).concat("/")
						.concat(sourceFile.getKey())));
			}
			listObjectsRequest.setMarker(sourceObjectsListing.getNextMarker());
		} while (sourceObjectsListing.isTruncated());

		Resource[] resources = resourceList.toArray(new Resource[resourceList.size()]);
		MultiResourceItemReader<RequestDataUpload> multiResourceItemReader = new MultiResourceItemReader<RequestDataUpload>();
		multiResourceItemReader.setName("RequestDataUpload-multiResource-Reader");
		multiResourceItemReader.setResources(resources);
		multiResourceItemReader.setDelegate(flatFileItemReader());
		synchronizedItemStreamReader.setDelegate(multiResourceItemReader);
		return synchronizedItemStreamReader;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {

	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}

	@Bean
	@StepScope
	public FlatFileItemReader<RequestDataUpload> flatFileItemReader() {
		FlatFileItemReader<RequestDataUpload> reader = new FlatFileItemReader<RequestDataUpload>();
		reader.setLinesToSkip(1);
		reader.setLineMapper(getLineMapper());
		return reader;
	}

	private DefaultLineMapper getLineMapper() {
		return new DefaultLineMapper() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(LINE_MAPPER);
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<RequestDataUpload>() {
					{
						setTargetType(RequestDataUpload.class);
					}
				});
			}
		};
	}

}
