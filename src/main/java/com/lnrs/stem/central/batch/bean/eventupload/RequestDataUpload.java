package com.lnrs.stem.central.batch.bean.eventupload;

import com.lnrs.stem.central.batch.bean.ResponseDataAddress;
import com.lnrs.stem.central.batch.bean.ResponseDataLexId;
import com.opencsv.bean.CsvBindByPosition;
import lombok.*;


/**
 * Raw event upload.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RequestDataUpload {

	private static final RequestDataUpload EMPTY = new RequestDataUpload();

	public static final String RESOLVER = "resolver";
	public static final String ADDRESS_LINE_1 = "addressLine1";

	public static final String ADDRESS_LINE_2 = "addressLine2";

	public static final String ADDRESS_LINE_3 = "addressLine3";

	public static final String ADDRESS_LINE_4 = "addressLine4";

	public static final String ADDRESS_LINE_5 = "addressLine5";

	public static final String ADDRESS_LINE_6 = "addressLine6";
	public static final String POSTAL_CODE = "postalCode";
	public static final String COUNTRY_CODE = "countryCode";

	public static final String LINE_OF_BUSINESS = "lineOfBusiness";
	public static final String POINT_OF_REQUEST = "pointOfRequest";
	public static final String INSURER_ABI_CODE = "insurerABICode";
	public static final String SCORE_THRE_SHOLD = "scoreThreshold";
	public static final String CRID_CONFIGURATION = "cridConfiguration";
	public static final String LEX_ID_ONLY = "lexIDOnly";
	public static final String TITLE = "title";
	public static final String FORENAME = "forename";

	public static final String MIDDLE_NAMES = "middleNames";
	public static final String SURNAME = "surname";
	public static final String DATE_OF_BIRTH = "dateOfBirth";

	public ResponseDataAddress responseDataAddress;

	public ResponseDataLexId responseDataLexId;

	@CsvBindByPosition(position = 0)
	private String resolver;

	@CsvBindByPosition(position = 1)
	private String addressLine1;

	@CsvBindByPosition(position = 2)
	private String addressLine2;

	@CsvBindByPosition(position = 3)
	private String addressLine3;

	@CsvBindByPosition(position = 4)
	private String addressLine4;

	@CsvBindByPosition(position = 5)
	private String addressLine5;

	@CsvBindByPosition(position = 6)
	private String addressLine6;

	@CsvBindByPosition(position = 7)
	private String postalCode;

	@CsvBindByPosition(position = 8)
	private String countryCode;

	@CsvBindByPosition(position = 9)
	private String lineOfBusiness;

	@CsvBindByPosition(position = 10)
	private String pointOfRequest;

	@CsvBindByPosition(position = 11)
	private String insurerABICode;

	@CsvBindByPosition(position = 12)
	private Integer scoreThreshold;

	@CsvBindByPosition(position = 13)
	private String cridConfiguration;

	@CsvBindByPosition(position = 14)
	private String lexIDOnly;

	@CsvBindByPosition(position = 15)
	private String title;

	@CsvBindByPosition(position = 16)
	private String forename;

	@CsvBindByPosition(position = 17)
	private String middleNames;

	@CsvBindByPosition(position = 18)
	private String surname;

	@CsvBindByPosition(position = 19)
	private String dateOfBirth;

	public boolean toArray() {
		return responseDataLexId.toArray().add(responseDataAddress.getResolvedAddress().getUdprn().toString());
	}
}