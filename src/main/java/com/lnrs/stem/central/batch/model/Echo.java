package com.lnrs.stem.central.batch.model;

import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This simple Echo entity class just serves as demo example resource 
 * used by EchoController as part of stem request/response model.
 * This is to be deleted and replaced by application entities.
 */

@Schema(description = "Simple class with echo String variable.")
public class Echo implements Serializable{

	private static final long serialVersionUID = 7057966307287885465L;

	@Schema(description = "The Echo string")
	private String echo;

	public Echo() {}

	public Echo(String echo) {
		this.echo = echo;
	}

	public String getEcho() {
		return echo;
	}

	public void setEcho(String echo) {
		this.echo = echo;
	}

	@Override
	public String toString() {
		return "Echo [echo=" + echo + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((echo == null) ? 0 : echo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Echo other = (Echo) obj;
		if (echo == null) {
			if (other.echo != null)
				return false;
		} else if (!echo.equals(other.echo))
			return false;
		return true;
	}

}
