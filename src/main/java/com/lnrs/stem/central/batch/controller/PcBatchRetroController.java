package com.lnrs.stem.central.batch.controller;

import com.lnrs.stem.api.models.StemRequest;
import com.lnrs.stem.api.models.StemResponse;
import com.lnrs.stem.central.batch.bean.RequestDataLexId;
import com.lnrs.stem.central.batch.bean.ResponseDataLexId;
import com.lnrs.stem.central.batch.service.DataSourceCaller;
import com.lnrs.stem.central.batch.service.RequestProcessorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("batch-retro")
public class PcBatchRetroController {

	static final String SERVER_PORT = "local.server.port";

	@Autowired
	Environment environment;

	@Autowired
	JobLauncher asyncJobLauncher;

	@Autowired
	JobOperator asyncJobOperator;

	@Autowired
	JobExplorer jobExplorer;

	@Autowired
	RequestProcessorService service;

	@Autowired
	DataSourceCaller dataSourceCaller;

	/* -------------------------------
	 * GET Operations
	 * -------------------------------*/

	@RequestMapping(value = "/lexid", method = RequestMethod.POST)
	public ResponseEntity<StemResponse<ResponseDataLexId>> getLexId(@RequestBody StemRequest<RequestDataLexId> requestDataLexId) {
		log.info("Tran quan lam");
		return ResponseEntity.ok().body(dataSourceCaller.callLexIdService(requestDataLexId));
	}

	@RequestMapping(value = "/job-address-and-lexid", method = RequestMethod.POST)
	public void submit(@RequestParam("file") String file) throws Exception {
		service.processRequestFileCSV(file);
		log.info("TRAN QUAN ALM");
	}
}
