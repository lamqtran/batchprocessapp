package com.lnrs.stem.central.batch.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestProcessorServiceImpl implements RequestProcessorService {

	private final DataSourceCaller dataSourceCaller;

	@Autowired
	JobExplorer jobExplorer;

	@Autowired
	Job jobProcess;

	@Autowired
	JobLauncher asyncJobLauncher;

	@Override
	public JobExecution processRequestFileCSV(String file) throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException {
		JobParameters jobParameters = new JobParametersBuilder(jobExplorer)
				.addString("pathFile", file)
				.getNextJobParameters(jobProcess)
				.toJobParameters();
		JobExecution execution = asyncJobLauncher.run(jobProcess, jobParameters);
		log.info("TRAN QUAN ALM " + jobProcess);
		return execution;
	}
}
