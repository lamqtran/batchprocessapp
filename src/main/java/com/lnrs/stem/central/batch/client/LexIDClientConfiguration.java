package com.lnrs.stem.central.batch.client;

import io.netty.handler.logging.LogLevel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

@Slf4j
@Configuration
public class LexIDClientConfiguration {

	@Bean
	public WebClient webServiceLexId(@Value("${servicelinks.lexid.url}") String urlServiceLexId, ReactorClientHttpConnector reactorClientHttpConnector) {
		HttpClient httpClient = HttpClient.create()
				.wiretap(this.getClass().getCanonicalName(), LogLevel.INFO, AdvancedByteBufFormat.TEXTUAL);
		ClientHttpConnector conn = new ReactorClientHttpConnector(httpClient);

		return WebClient
				.builder()
				.clientConnector(conn)
				.baseUrl(urlServiceLexId)
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.build();
	}

	@Bean
	public WebClient webServiceAddress(@Value("${servicelinks.address.url}") String urlServiceAddress, ReactorClientHttpConnector reactorClientHttpConnector) {
		HttpClient httpClient = HttpClient.create()
				.wiretap(this.getClass().getCanonicalName(), LogLevel.INFO, AdvancedByteBufFormat.TEXTUAL);
		ClientHttpConnector conn = new ReactorClientHttpConnector(httpClient);

		return WebClient
				.builder()
				.clientConnector(conn)
				.baseUrl(urlServiceAddress)
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.build();
	}
}
