package com.lnrs.stem.central.batch.bean.ctrladdress;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Component {

	private Object name;

	private Object number;

	private Object street;

	private Object town;

	private Object district;

	private Object county;

	private Object postcode;

	private Object country;
}
