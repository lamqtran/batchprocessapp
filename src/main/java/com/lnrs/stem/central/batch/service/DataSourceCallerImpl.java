package com.lnrs.stem.central.batch.service;

import com.lnrs.stem.api.models.StemRequest;
import com.lnrs.stem.api.models.StemResponse;
import com.lnrs.stem.central.batch.bean.RequestDataLexId;
import com.lnrs.stem.central.batch.bean.ResponseDataLexId;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@RequiredArgsConstructor
public class DataSourceCallerImpl implements DataSourceCaller {
	@Autowired
	WebClient webServiceLexId;


	@Override
	public StemResponse<ResponseDataLexId> callLexIdService(StemRequest<RequestDataLexId> requestDataLexId) {

		StemResponse<ResponseDataLexId> stemResponseResponseEntity = webServiceLexId.post()
				.bodyValue(requestDataLexId)
				.headers(headers -> headers.setBearerAuth("eyJraWQiOiIxcmVhQ080K1FvZGtJandGVmJlXC9uMjJPcm9lVG9ha2txVFIxR0t5ZDFXYz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxM3RodHVoM2lkcWMwbDduY2diMmw5Z2gyaiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoic3RlbS1jZW50cmFsLWxleGlkXC9hcGkuZnVsbCIsImF1dGhfdGltZSI6MTY2NDgxMTE2NCwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmV1LXdlc3QtMS5hbWF6b25hd3MuY29tXC9ldS13ZXN0LTFfeEJiaXJrOThhIiwiZXhwIjoxNjY0ODE0NzY0LCJpYXQiOjE2NjQ4MTExNjQsInZlcnNpb24iOjIsImp0aSI6IjRmMWRiZjU3LTRiYTMtNGNiOS1iZWE4LTZmY2ZlMmM5MWQ0NyIsImNsaWVudF9pZCI6IjEzdGh0dWgzaWRxYzBsN25jZ2IybDlnaDJqIn0.zJXWZ-LfNZUNEDbUxCafWNIUmq-3Y7PvJDQky5jxkye40r91_91Ct8ofy48t5TN8kP4OBVXFX_0GSGpdfjptJ1SS4J_IbGqmawHb-Amrialm6klbJXPuspPUekpXUp0oUH-mR1dSV14Lko0_0amSfSnKebutBFuxwGaHseEjGmPa7yxrLNpOsuhYBNmUJQGZJaTFOv-xyfKpzoMIr9Es6qJ5jvHl-Kg9xyMYiEvmgyvxHDpf6nWzmH3e5Ltz7mP41vFdNUcvOcQ_GLbIvs3wZDFb4UcLKJs4JbmwhV2nRFfSW1gZ6RvZq3roJVvndBbz_84cfWJkeRcR7Int8oYpbA"))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(StemResponse.class)
				.block();

		return stemResponseResponseEntity;
	}
}
