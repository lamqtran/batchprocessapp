package com.lnrs.stem.central.batch.api.configuration;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

/**
 * Swagger configuration class. For details refer to
 * https://confluence.rsi.lexisnexis.com/display/UKIRE/Sample+Service+-+API+Documentation
 * 
 */
@Configuration
public class SwaggerConfiguration {

	public static final String VERSION_1 = "1";

	@Bean
	public GroupedOpenApi api() {
		return GroupedOpenApi.builder().group(VERSION_1).pathsToMatch("/**").packagesToScan("com.lnrs").build();
	}
	

	@Bean
	public OpenAPI springOpenAPI() {
		return new OpenAPI().info(new Info().title("STEM - Service Template API")
				.description("Generic API for Stem Microservices").version(VERSION_1)
				.contact(new Contact().email("example@lexisnexisrisk.com")));
	}
}