package com.lnrs.stem.central.batch.bean.ctrllexid;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Name {

	private String title;

	private String forename;

	private List<String> middleNames;

	private String surname;
}
