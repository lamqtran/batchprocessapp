package com.lnrs.stem.central.batch.job.process;


import lombok.*;
import org.springframework.batch.core.BatchStatus;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobResponseDTO {
    Long jobId;
    BatchStatus status;
    String name;
    Map<String, Object> params = new HashMap();
    Date startTime;
    Date endTime;
    List<String> errors;
}

