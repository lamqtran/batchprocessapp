package com.lnrs.stem.central.batch.bean.ctrladdress;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Address {

	private List<String> lines;

	private String postalCode;

	private String countryCode;
}
