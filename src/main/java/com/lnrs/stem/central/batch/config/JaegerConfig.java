package com.lnrs.stem.central.batch.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.jaegertracing.internal.JaegerTracer;
import io.jaegertracing.internal.MDCScopeManager;

@Configuration
public class JaegerConfig {

	@Value("${spring.application.name}")
	private String appName;

	@Bean
	// https://github.com/jaegertracing/jaeger-client-java/blob/master/jaeger-core/README.md
    public JaegerTracer  mdcJaegerTracerBuilder() {
		MDCScopeManager scopeManager = new MDCScopeManager.Builder().build();
		return new JaegerTracer.Builder(appName).withScopeManager(scopeManager).build();
    }
}
