package com.lnrs.stem.central.batch.bean.ctrladdress;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Configuration {

	private String resolver;
}
