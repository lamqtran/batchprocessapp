package com.lnrs.stem.central.batch.bean.ctrllexid;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Configuration {

    private String resolver;

    private String lineOfBusiness;

    private String pointOfRequest;

    private String insurerABICode;

    private Integer scoreThreshold;

    private String cridConfiguration;

    private String lexIDOnly;
}
