package com.lnrs.stem.central.batch.bean.armaddrcleaner;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ARMAddressResponse {

	@JsonProperty("hwo_address")
	private HWOAddress hwoAddress;

	@JsonProperty("hwo_postcode_lookup")
	private String hwoPostcodeLookup;

	@JsonProperty("hwo_elements")
	private Integer hwoElements;

	@JsonProperty("hwo_cache")
	private String hwoCache;

	@JsonProperty("hwo_address_date")
	private String hwoAddressDate;

	@JsonProperty("hwo_address_status")
	private String hwoAddressStatus;

}
