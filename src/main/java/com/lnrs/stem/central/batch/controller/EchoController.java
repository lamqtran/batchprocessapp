package com.lnrs.stem.central.batch.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lnrs.stem.api.controller.StemController;
import com.lnrs.stem.api.models.StemRequest;
import com.lnrs.stem.api.models.StemResponse;
import com.lnrs.stem.central.batch.controller.api.EchoApi;
import com.lnrs.stem.central.batch.model.Echo;

/**
 * This controller class just serves as a simple echo-back controller.
 * This should deleted and replaced by application specific controller.
 */

@RestController
public class EchoController extends StemController implements EchoApi{

	private static final Logger logger = LoggerFactory.getLogger(EchoController.class);
	
	public EchoController() {}

	@Override
	@GetMapping(path = "/echo/{echo}")
	public ResponseEntity<StemResponse<Echo>> echo(@Valid @PathVariable String echo) throws Exception {
		logger.info("GET request with URL path param of: " + echo);
		StemResponse<Echo> stemResp = createStemResponse(new Echo(echo));
		return ResponseEntity.status(HttpStatus.OK).body(stemResp);
	}
	
	@Override
	@PostMapping(path = "/echo")
	public ResponseEntity<StemResponse<Echo>> echo(@Valid @RequestBody StemRequest<Echo> echo) throws Exception {
		logger.info("POST request with body of: " + echo);
		StemResponse<Echo> stemResp = createStemResponse(echo.getContent());
		return ResponseEntity.status(HttpStatus.OK).body(stemResp);
	}
}
