package com.lnrs.stem.central.batch.bean;

import com.lnrs.stem.central.batch.bean.ctrladdress.Address;
import com.lnrs.stem.central.batch.bean.ctrladdress.Configuration;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@Builder
public class RequestDataAddress {

	private Configuration configuration;

	private Address address;

}
