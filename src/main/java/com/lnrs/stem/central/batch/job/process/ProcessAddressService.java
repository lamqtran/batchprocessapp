package com.lnrs.stem.central.batch.job.process;

import com.lnrs.stem.api.models.StemRequest;
import com.lnrs.stem.api.models.StemResponse;
import com.lnrs.stem.central.batch.bean.RequestDataAddress;
import com.lnrs.stem.central.batch.bean.ResponseDataAddress;
import com.lnrs.stem.central.batch.bean.config.TokenConfig;
import com.lnrs.stem.central.batch.bean.ctrladdress.Address;
import com.lnrs.stem.central.batch.bean.ctrladdress.Configuration;
import com.lnrs.stem.central.batch.bean.eventupload.RequestDataUpload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;

//AddressProcessor


@Slf4j
@Component
public class ProcessAddressService implements ItemProcessor<RequestDataUpload, RequestDataUpload> {

	@Autowired
	WebClient webServiceAddress;

	@Autowired
	TokenConfig tokenConfig;

	@Override
	public RequestDataUpload process(RequestDataUpload requestDataUpload) throws Exception {
		log.info("ProcessAddressService");

		Configuration configuration = Configuration.builder().resolver("addressUK").build();

		List<String> lines = Arrays.asList(requestDataUpload.getAddressLine1(),
				requestDataUpload.getAddressLine2(),
				requestDataUpload.getAddressLine3(),
				requestDataUpload.getAddressLine4(),
				requestDataUpload.getAddressLine5(),
				requestDataUpload.getAddressLine6());

		Address address = Address.builder()
				.countryCode(requestDataUpload.getCountryCode())
				.postalCode(requestDataUpload.getPostalCode())
				.lines(lines)
				.build();

		RequestDataAddress requestDataAddress = RequestDataAddress.builder()
				.configuration(configuration)
				.address(address)
				.build();


		StemRequest<RequestDataAddress> requestDataAddressStemRequest = new StemRequest<>();
		requestDataAddressStemRequest.setContent(requestDataAddress);
		requestDataAddressStemRequest.setCaller("precisionClaimsMotorIngest");

		StemResponse<ResponseDataAddress> stemResponseResponseEntity = webServiceAddress.post()
				.bodyValue(requestDataAddressStemRequest)
				.headers(headers -> headers.setBearerAuth(tokenConfig.getAddress()))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(new ParameterizedTypeReference<StemResponse<ResponseDataAddress>>() {
				})
				.block();

		ResponseDataAddress responseDataAddress = stemResponseResponseEntity.getContent();

//		String jsonString = new JSONObject(stemResponseResponseEntity.getContent()).toString();
//		ResponseDataAddress ResponseDataAddress = objectMapper.readValue(jsonString, ResponseDataAddress.class);

		requestDataUpload.setResponseDataAddress(responseDataAddress);
		return requestDataUpload;
	}
}
