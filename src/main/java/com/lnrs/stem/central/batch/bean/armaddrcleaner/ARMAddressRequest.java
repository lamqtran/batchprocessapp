package com.lnrs.stem.central.batch.bean.armaddrcleaner;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ARMAddressRequest {

	private String addressLine1;

	private String addressLine2;

	private String addressLine3;

	private String addressLine4;

	private String addressLine5;

	private String addressLine6;

	private String postcode;
}
