package com.lnrs.stem.central.batch.job.process;

import com.lnrs.stem.api.models.StemRequest;
import com.lnrs.stem.api.models.StemResponse;
import com.lnrs.stem.central.batch.bean.RequestDataLexId;
import com.lnrs.stem.central.batch.bean.ResponseDataLexId;
import com.lnrs.stem.central.batch.bean.config.TokenConfig;
import com.lnrs.stem.central.batch.bean.ctrladdress.Address;
import com.lnrs.stem.central.batch.bean.ctrllexid.Configuration;
import com.lnrs.stem.central.batch.bean.ctrllexid.Name;
import com.lnrs.stem.central.batch.bean.eventupload.RequestDataUpload;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
//LexIdProcessor
public class ProcessLexIdService implements ItemProcessor<RequestDataUpload, RequestDataUpload> {

	@Autowired
	WebClient webServiceLexId;

	@Autowired
	TokenConfig tokenConfig;

	@Override
	public RequestDataUpload process(RequestDataUpload requestDataUpload) throws Exception {

		Configuration configuration = Configuration.builder()
				.cridConfiguration(requestDataUpload.getCridConfiguration())
				.insurerABICode(requestDataUpload.getInsurerABICode())
				.lexIDOnly(requestDataUpload.getLexIDOnly())
				.lineOfBusiness(requestDataUpload.getLineOfBusiness())
				.pointOfRequest(requestDataUpload.getPointOfRequest())
				.resolver(requestDataUpload.getResolver())
				.scoreThreshold(requestDataUpload.getScoreThreshold())
				.build();

		String middleName = requestDataUpload.getMiddleNames();

		Name name = Name.builder()
				.forename(requestDataUpload.getForename())
				.surname(requestDataUpload.getSurname())
				.title(requestDataUpload.getTitle())
				.middleNames(StringUtils.isNotEmpty(middleName) ? Arrays.asList(StringUtils.split(middleName, " ")) : null)
				.build();

		List<String> lines = Arrays.asList(requestDataUpload.getAddressLine1(),
				requestDataUpload.getAddressLine2(),
				requestDataUpload.getAddressLine3(),
				requestDataUpload.getAddressLine4(),
				requestDataUpload.getAddressLine5(),
				requestDataUpload.getAddressLine6());

		Address address = Address.builder()
				.countryCode(requestDataUpload.getCountryCode())
				.postalCode(requestDataUpload.getPostalCode())
				.lines(lines)
				.build();


		RequestDataLexId requestDataLexId = RequestDataLexId.builder()
				.configuration(configuration)
				.name(name)
				.address(address)
				.dateOfBirth(requestDataUpload.getDateOfBirth())
				.build();

		StemRequest<RequestDataLexId> requestDataLexIdStemRequest = new StemRequest<>();
		requestDataLexIdStemRequest.setContent(requestDataLexId);
		requestDataLexIdStemRequest.setCaller("datahub");

		StemResponse<ResponseDataLexId> stemResponseResponseEntity = webServiceLexId.post()
				.bodyValue(requestDataLexIdStemRequest)
				.headers(headers -> headers.setBearerAuth(tokenConfig.getLexId()))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(new ParameterizedTypeReference<StemResponse<ResponseDataLexId>>() {
				})
				.block();

		ResponseDataLexId responseDataLexId = stemResponseResponseEntity.getContent();

		requestDataUpload.setResponseDataLexId(responseDataLexId);
		return requestDataUpload;
	}
}
