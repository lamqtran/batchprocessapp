package com.lnrs.stem.central.batch.bean.ctrladdress;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResolvedAddress {

	private Object udprn;

	private Component components;

	private Lines lines;
}
