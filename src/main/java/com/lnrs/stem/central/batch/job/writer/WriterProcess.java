package com.lnrs.stem.central.batch.job.writer;


import com.lnrs.stem.central.batch.bean.ResponseDataAddress;
import com.lnrs.stem.central.batch.bean.ResponseDataLexId;
import com.lnrs.stem.central.batch.bean.eventupload.RequestDataUpload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Slf4j
//ResponseWriter
public class WriterProcess implements ItemWriter<RequestDataUpload>, StepExecutionListener {
	@Override
	public void write(List<? extends RequestDataUpload> messages) {
		messages.forEach(requestDataUpload -> {
			List<String> listFieldRequestData = requestDataUpload.getResponseDataLexId().toArray();
			ResponseDataAddress dataAddress = requestDataUpload.getResponseDataAddress();
			ResponseDataLexId dataLexId = requestDataUpload.getResponseDataLexId();
			try {
				if (!Objects.isNull(dataAddress) && !Objects.isNull(dataAddress.getResolvedAddress()))
					listFieldRequestData.add(requestDataUpload.getResponseDataAddress().getResolvedAddress().getUdprn().toString());
				listFieldRequestData.add(dataLexId.getLexID());
				listFieldRequestData.add(dataLexId.getConfidenceScore());
				String[] itemsArray = new String[listFieldRequestData.size()];
				givenDataArray_whenConvertToCSV_thenOutputCreated(listFieldRequestData.toArray(itemsArray));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		});
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {

	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}

	public void givenDataArray_whenConvertToCSV_thenOutputCreated(String[] dataLines) throws IOException {
		File csvOutputFile = new File("Output");
		try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
			String lines = this.convertToCSV(dataLines);
			pw.println(lines);
		}
//        assertTrue(csvOutputFile.exists());
	}

	public String convertToCSV(String[] data) {
		return Stream.of(data)
				.map(this::escapeSpecialCharacters)
				.collect(Collectors.joining(","));
	}

	public String escapeSpecialCharacters(String data) {
		String escapedData = data.replaceAll("\\R", " ");
		if (data.contains(",") || data.contains("\"") || data.contains("'")) {
			data = data.replace("\"", "\"\"");
			escapedData = "\"" + data + "\"";
		}
		return escapedData;
	}
}
