package com.lnrs.stem.central.batch.bean.armaddrcleaner;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HWOAddress {

	@JsonProperty("hwo_line")
	private Object hwoLine;
	@JsonProperty("hwo_key")
	private Object hwoKey;
	@JsonProperty("hwo_name")
	private Object hwoName;
	@JsonProperty("hwo_number")
	private Object hwoNumber;
	@JsonProperty("hwo_street")
	private Object hwoStreet;
	@JsonProperty("hwo_town")
	private Object hwoTown;
	@JsonProperty("hwo_district")
	private Object hwoDistrict;
	@JsonProperty("hwo_county")
	private Object hwoCounty;
	@JsonProperty("hwo_postcode")
	private Object hwoPostcode;
	@JsonProperty("hwo_country")
	private Object hwoCountry;
	@JsonProperty("hwo_address_line_1")
	private Object hwoAddressLine1;
	@JsonProperty("hwo_address_line_2")
	private Object hwoAddressLine2;
	@JsonProperty("hwo_address_line_3")
	private Object hwoAddressLine3;
	@JsonProperty("hwo_address_line_4")
	private Object hwoAddressLine4;
	@JsonProperty("hwo_address_line_5")
	private Object hwoAddressLine5;
	@JsonProperty("hwo_commercial")
	private Object hwoCommercial;
	@JsonProperty("hwo_organisation")
	private Object hwoOrganisation;
	@JsonProperty("hwo_easting")
	private Object hwoEasting;
	@JsonProperty("hwo_northing")
	private Object hwoNorthing;

}
