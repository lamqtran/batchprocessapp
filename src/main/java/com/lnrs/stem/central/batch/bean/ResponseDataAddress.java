package com.lnrs.stem.central.batch.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lnrs.stem.central.batch.bean.ctrladdress.ResolvedAddress;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDataAddress {

	private String productVersion;

	private String status;

	private String message;

	private String source;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private ResolvedAddress resolvedAddress;
}
