# Using  HELM

This README file will need to be amended so as to be specific to your Application.

## Contains basic steps how to :
 1. Create helm charts and install service using helm
 1. Release helm chart to nexus repository
 1. Deploy from remote helm repository


## Preconditions
 - docker installed
 - kubernetes installed and context set to cluster (e.g. minikube)
 - helm installed ([https://helm.sh/docs/using_helm/])
 - docker image built and pushed it into registry of your choice. If you do not have your registry set up, use gitlab one - follow instructions in Registry menu in your gitlab project [https://gitlab.ins.risk.regn.net/UKI/systems/my-group/my-project/container_registry]
 - access to helm charts repository [http://nexus.mapview.eu.lnrm.net/content/repositories/service_templates_helm_charts]

## Amend helm charts and install service using helm
 1. Change parent directory name `project-template` to your service name (`SERVICE_NAME`)
 1. Edit Chart.yaml change name to `SERVICE_NAME` and set appropriate `VERSION`
 1. Edit `environments/global.values.yaml`
    - set `chart.name` to `SERVICE_NAME`
    - set `chart.version` to `VERSION`
 1. Edit values.yaml
    - make sure image.repository is pointing to docker image (e.g. `repository: "gitlab.ins.risk.regn.net:4567/uki/systems/my-group/my-project"`)
 1. Check if helm configuration is valid `$ helm lint ./<chartName>`
 - Review results of helm build: `$ helm lint ./$SERVICE_NAME`

## Release helm chart to nexus repository
1. create package in directory of your choice:
    ```	
    $ mkdir public 
    $ helm package . --destination ./public
    ```
1. push package to repository
    ```
    $ read NEXUS_USERNAME;
    $ read -s NEXUS_PASSWORD;
    $ curl -u ${NEXUS_USERNAME}:${NEXUS_PASSWORD} --upload-file ./public/"$(ls public | grep tgz | head -n 1)" http://nexus.mapview.eu.lnrm.net/content/repositories/service_templates_helm_charts/incubator/
    ```
1. Download index file from remote repository and merge it with new one including your chart
    ```
    $ wget http://nexus.mapview.eu.lnrm.net/content/repositories/service_templates_helm_charts/incubator/index.yaml
    $ helm repo index public --url http://nexus.mapview.eu.lnrm.net/content/repositories/service_templates_helm_charts/incubator --merge index.yaml
    ```
1. push index file in your repository
    ```
    $ curl -u ${NEXUS_USERNAME}:${NEXUS_PASSWORD} --upload-file ./public/index.yaml http://nexus.mapview.eu.lnrm.net/content/repositories/service_templates_helm_charts/incubator/
    ```
## Deploy from helm repository
1. Add the following environment variables

    | Variable                | Example value                                                                                  | Description                                                      |
    | ----------------------- | ---------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- |
    | SERVICE_NAMESPACE       | ins-stem                                                                                       | The namespace to deploy the service into                         |
    | RELEASE_ENV_NAME        | test                                                                                           | A suffix to be added to the release name                         |
    | CLUSTER_NAME            | eu-mesh-poc-eu-west-1.aws.dev.ins.lnrs.io                                                      | The identifier of the cluster to deploy to in kube configuration |

1. Run `helmfile.exe --environment dev --file path/to/helmfile.yaml` apply
