{{- $deployment := .Values -}}
{{- $fullName := include "application.fullname" . -}}
{{- $name := include "application.name" . -}}
{{- $chart := include "application.chart" . -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $fullName }}
  labels:
     app.kubernetes.io/name: {{ $chart }}
     app.kubernetes.io/instance: {{ $fullName }}
     app:  {{ $fullName }}
     chart: {{ $chart }}
     tag: {{ template "application.hashTag" . }}
spec:
  replicas: {{ .Values.service.application.replicaCount }}
  selector:
    matchLabels:
      app:  {{ $fullName }}
  template:
    metadata:
      labels:
        app:  {{ $fullName }}
        release: {{ $fullName }}
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        sidecar.istio.io/rewriteAppHTTPProbers: "true"
    spec:
{{- if .Values.regsecret.enabled }}
      imagePullSecrets:
        - name: {{ .Values.regsecret.stem.registry.pullSecretName }}
{{- end }}
      volumes:
        - name: {{ $fullName }}
          configMap:
            name: {{ $fullName }}
            defaultMode: 420
      containers:
        - name: {{ $name }}
          image: {{ .Values.service.application.image.repository }}:{{ .Values.service.application.image.tag }}
          imagePullPolicy: {{ .Values.service.application.image.pullPolicy }}
          ports:
{{- range .Values.service.application.ports }}
  {{- range .mappings }}
            - name: {{ .name }}
              containerPort: {{ .containerPort }}
              protocol: {{ .protocol | default "TCP" }}
  {{- end }}
{{- end }}
          env:
{{- range $name, $config := $deployment.service.application.env }}
            - name: {{ $name | snakecase | upper }}
{{- if $config.secret }}
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-%s" ( default "" $config.secretName ) $fullName | trimPrefix "-" }}
                  key: {{ default $name $config.key }}
{{- else if $config.fullName }}
              value: {{ $fullName }}
{{- else }}
              value: {{ $config.static }}
{{- end }}
{{- end }}
{{- if .Values.service.application.probes.health.enabled }}
          livenessProbe:
            httpGet:
              path: {{ printf "%s/%s" ( trimSuffix "/" .Values.application.contextPath ) ( trimPrefix "/" .Values.service.application.probes.health.endpoint ) }}
              port: {{ default .Values.service.application.containerPort .Values.service.application.probes.health.port }}
            initialDelaySeconds: {{ .Values.service.application.probes.health.initialDelaySeconds }}
            periodSeconds: {{ .Values.service.application.probes.health.periodSeconds }}
            timeoutSeconds: {{ .Values.service.application.probes.health.timeoutSeconds }}
{{- end }}
{{- if .Values.service.application.probes.readiness.enabled }}
          readinessProbe:
            httpGet:
              path: {{ .Values.application.contextPath | trimSuffix "/" }}/actuator/health
              port: {{ default .Values.service.application.containerPort .Values.service.application.probes.readiness.port }}
            initialDelaySeconds: {{ .Values.service.application.probes.readiness.initialDelaySeconds }}
            periodSeconds: {{ .Values.service.application.probes.readiness.periodSeconds }}
            timeoutSeconds: {{ .Values.service.application.probes.readiness.timeoutSeconds }}
{{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - name: {{ $fullName }}
              readOnly: true
              mountPath: /usr/local/config
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}